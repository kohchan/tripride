# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:


１アプリの概要
　　自分の旅行体験を投稿するアプリである

２アプリケーションの機能一覧
　　記事投稿機能
　　ログイン機能
　　管理ユーザーログイン機能
　　ページネーション機能
　　画像アップロード機能
　　フォローフォロワー機能
　　いいね機能
　　ユーザー　投稿検索機能
　　
３技術
　　デプロイ　Heroku
　　データーベース　PostgreSQL
　　画像アップロード　S3
　　検索機能　ransack
　　
