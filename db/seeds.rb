# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


User.create!(name:  "管理者",
             email: "exampleuser@gmail.com",
             password:              "tripexample",
             password_confirmation: "tripexample",
             admin: true)
             
User.create!(name: "サンプルユーザー",
             email: "sampleuser@gmail.com",
             password:             "tripsample",
             password_confirmation: "tripsample")
User.create!(name: "こうすけ",
             email: "kousuke@gmail.com",
             password: "tripkousuke",
             password_confirmation: "tripkousuke")
User.create!(name: "ゆーたろー",
             email: "yutaro@gmail.com",
             password: "tripyutaro",
             password_confirmation: "tripyutaro")

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password)
end

users = User.order(:created_at).take(6)

# リレーションシップ
users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }